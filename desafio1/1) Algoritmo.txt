INICIO
1- Encender la PC.
2- Abrir nuestro navegador.
3- Buscar la p�gina web de Cadif1.
4- Si la sesi�n no est� iniciada, iniciarla.
5- Buscar nuestra secci�n de la materia.
6- Dar click en el bot�n verde de la secci�n.
7- Buscar el apartado "Desaf�os".
8- Darle click.
9- Al visualizar el desaf�o, dar click en "Ver enunciado".
10- Leer las instrucciones detalladamente para realizar el desaf�o.
11- Realizar el desaf�o en el tipo de archivo que nos pidan.
12- Guardar el archivo en la PC.
13- Si se debe comprimir, comprimirlo.
14- En la p�gina de cadif1, buscar "Cargar soluci�n".
15- Darle click.
16- Subir el archivo del desaf�o.
FIN

